# StopLoss/TakeProfit
## Technologies used
- Akka HTTP
- Circe
- Canoe
- Slick (in process)
- SQLite (replace)

## Commands
- `/portfolio` - shows your portfolio
- `/balance` - shows your currencies balance
- `/searchbyticket` - searches market's instruments
- `/orderbook` - shows orderbook by figi
- `candles` - shows candles (fix it)
- `/marketorder` - create market order by figi
- `/limitorder` - create limit order by figi
- `/stoplosstakeprofit` - create stoploss takeprofit order (in process)
- `/orders` - shows your active orders
- `/cancelorder` - cancel order by id
- `/help` - show all commands
## ToDo
- Регистрация пользователя `/register`
- SLTP
- Работа со streaming
- Запись в БД
## Links
- [Tinkoff API DOC](https://tinkoffcreditsystems.github.io/invest-openapi/)
- [Canoe](https://github.com/augustjune/canoe)
- [Akka HTTP](https://doc.akka.io/docs/akka-http/current/)
- [Circe](https://circe.github.io/circe/)